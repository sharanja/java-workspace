package bcas.ap.start;

public class StaticBlock {

	static {// static block will invoke before main method
			System.out.println("Hi from static method");
		}
	public static void main(String[] args) {
		System.out.println("Welcome!!!");
	}
}